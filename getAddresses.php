<?php
// Open the website database
function openDatabase( $dppF )
{

    $dbhandle = mysql_connect( base64_decode( $dppF[0] ),
			       base64_decode( $dppF[1] ),
			       base64_decode( $dppF[2] ) );
    return $dbhandle;
}

// Get counties and EC calls and names
function getCounties( $district, $dbhandle )
{
    $SQL="SELECT A.`COUNTYNAME`,A.`ECCALL`,B.`NAME` " .
        "FROM `arpsc_counties` A, `calldirectory` B " . 
        "WHERE `DISTRICT`=" . $district  . " " .
        "AND A.`ECCALL` = B.`CALLSIGN` " .
	"ORDER BY `COUNTYNAME`;";
    $result=mysql_query($SQL,$dbhandle);
    return $result;
}

// Get the email address for an EC
function getEmail( $call, $dbhandle )
{
    $SQL3 = "SELECT `contact` FROM `ares_contact_info` " .
	"WHERE `call`='" . $call . "' AND `type`=4 " .
	"ORDER BY `validity` DESC;";
    $result3=mysql_query($SQL3,$dbhandle);
    return $result3;
}

// Show the name and email for all ECs in a district
function doAdistrict( $district, $dbhandle )
{
    echo "\n-------- D i s t r i c t   " . $district . " ----------\n";
    $result1=getCounties($district,$dbhandle);
    while ( $row1=mysql_fetch_row($result1) )
	{
	    echo "\"" . $row1[0] . ": " . $row1[2] . ", " . $row1[1] . 
		"\" ";
	    $result2=getEmail($row1[1],$dbhandle);
	    if ( $row2=mysql_fetch_row($result2) )
		{
		    echo "<" . $row2[0] . ">";
		}
	    echo "\n";
	}
}

// Mainline, show emails for all non-NWS EC's by district
{
    date_default_timezone_set('America/Detroit');

    // Database credentials
    $dppF[0]="Y2ltYmFvdGg=";
    $dppF[1]="bWlfbnRzX2RiX3VzZXI=";
    $dppF[2]="c2xhcnR5YmFhcmRmYXJzdA==";

    // Open the database
    $dbhandle = openDatabase($dppF);
    mysql_select_db("mi-nts_org_-_website",$dbhandle);

    // Do each of the districts
    doAdistrict(1,$dbhandle);
    doAdistrict(2,$dbhandle);
    doAdistrict(3,$dbhandle);
    doAdistrict(5,$dbhandle);
    doAdistrict(6,$dbhandle);
    doAdistrict(7,$dbhandle);
    doAdistrict(8,$dbhandle);
}
?>